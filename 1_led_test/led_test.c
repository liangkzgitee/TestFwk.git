/*
 * Copyright (C) 2023 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_i2c.h"
#include "hi_io.h"

static void LedTask(void)
{
    printf("[LedTask] LedTask is running ...\n");
    //while (1)
    {
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 1);
        usleep(500*1000);
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 0);
        usleep(500*1000);
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 1);
        usleep(500*1000);
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 0);
        usleep(500*1000);
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 1);
        usleep(500*1000);
        IoTGpioSetOutputVal(HI_IO_NAME_GPIO_0, 0);
    }
}

void LedTestEntry(void)
{
    IoTGpioInit(HI_IO_NAME_GPIO_0);
    hi_io_set_func(HI_IO_NAME_GPIO_0, HI_IO_FUNC_GPIO_0_GPIO);
    IoTGpioSetDir(HI_IO_NAME_GPIO_0, IOT_GPIO_DIR_OUT);

#if 1
    LedTask();
#else
    //osPriorityAboveNormal[32], osPriorityNormal[24]
    //{.name, .attr_bits, .cb_mem, .cb_size, .stack_mem, .stack_size, .priority, .tz_module, .reserved}
    osThreadAttr_t attr = {"LedTask", 0, NULL, 0, NULL, 1024*2, 32, 0, 0};

    if (osThreadNew((osThreadFunc_t)LedTask, NULL, &attr) == NULL) {
        printf("[LedTestEntry] Falied to create %s!\n", attr.name);
    }
#endif
}
