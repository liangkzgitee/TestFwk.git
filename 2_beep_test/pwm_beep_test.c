/*
 * Copyright (C) 2023 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "hi_pwm.h"
#include "hi_io.h"


void BeepTask(void)
{
    int cnt = 3;
    printf("[BeepTask] BeepTask is running ...\n");
    while (cnt--) {
        IoTPwmStart(HI_PWM_PORT_PWM2, 50, 4000);
        usleep(500*1000);
        IoTPwmStop(HI_PWM_PORT_PWM2);
        usleep(300*1000);
    }
}

void BeepTestEntry(void)
{
    IoTGpioInit(HI_IO_NAME_GPIO_5);
    hi_io_set_func(HI_IO_NAME_GPIO_5, HI_IO_FUNC_GPIO_5_PWM2_OUT);
    IoTGpioSetDir(HI_IO_NAME_GPIO_5, IOT_GPIO_DIR_OUT);
    IoTPwmInit(HI_PWM_PORT_PWM2);

#if 1
    BeepTask();
#else
    //osPriorityAboveNormal[32], osPriorityNormal[24]
    //{.name, .attr_bits, .cb_mem, .cb_size, .stack_mem, .stack_size, .priority, .tz_module, .reserved}
    osThreadAttr_t attr = {"BeepTask", 0, NULL, 0, NULL, 1024*2, 32, 0, 0};

    if (osThreadNew((osThreadFunc_t)BeepTask, NULL, &attr) == NULL) {
        printf("[BeepTestEntry] Falied to create %s!\n", attr.name);
    }
#endif
}

