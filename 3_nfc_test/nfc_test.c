/*
 * Copyright (C) 2023 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_i2c.h"
#include "hi_io.h"

#include "ssd1306.h"

#include "nfc_config.h"
extern unsigned char nfc_mode;
static osThreadId_t g_NfcTaskId = NULL;

void DispayNfcInfo(void)
{
    ssd1306_Fill(Black);
    ssd1306_SetCursor(5, 5);
    ssd1306_DrawString("NFC", Font_11x18, White);
    ssd1306_SetCursor(5, 5+18+5);
    ssd1306_DrawString("Test......", Font_7x10, White);
    ssd1306_SetCursor(5, 5+18+5+10+5+10);
    ssd1306_DrawString("UP          DOWN", Font_7x10, White);
    ssd1306_UpdateScreen();
}

#define HI_I2C_IDX_BAUDRATE 400000 // 400k
/*i2c read*/
unsigned int write_read(unsigned char reg_high_8bit_cmd,
                        unsigned char reg_low_8bit_cmd,
                        unsigned char* recv_data,
                        unsigned char send_len,
                        unsigned char read_len)
{
    unsigned int ret;

    IotI2cIdx id = IOT_I2C_IDX_0;
    IotI2cData co8i_nfc_i2c_read_data ={0};
    IotI2cData c081nfc_i2c_write_cmd_addr ={0};

    unsigned char _send_user_cmd[2] = {reg_high_8bit_cmd, reg_low_8bit_cmd};

    memset(recv_data, 0x0, sizeof(recv_data));
    memset(&co8i_nfc_i2c_read_data, 0x0, sizeof(IotI2cData));

    c081nfc_i2c_write_cmd_addr.sendBuf = _send_user_cmd;
    c081nfc_i2c_write_cmd_addr.sendLen = send_len;

    co8i_nfc_i2c_read_data.receiveBuf = recv_data;
    co8i_nfc_i2c_read_data.receiveLen = read_len;

    IoTI2cWrite(id, C081_NFC_ADDR&0xFE, c081nfc_i2c_write_cmd_addr.sendBuf, c081nfc_i2c_write_cmd_addr.sendLen);
    IoTI2cRead(id, C081_NFC_ADDR|I2C_RD, co8i_nfc_i2c_read_data.receiveBuf, co8i_nfc_i2c_read_data.receiveLen);

    return 0;
}

void nfc_init(void)
{
    nfc_mode = 0;
 	Protocol_Config();
    FM11_Init();
}

void NfcTask(void)
{
    printf("[NfcTask] NfcTask is running ...\n");
    DispayNfcInfo();

    nfc_init();
    //while (1)
    //{
        nfcread();
    //    usleep(10000);
    //}

    g_NfcTaskId = NULL;
}

void NfcTestEntry(void)
{
    if (g_NfcTaskId) {
        return;
    }
    DispayNfcInfo();

    // GPIO6 will be init in FM11_Init()
    IoTGpioInit(HI_IO_NAME_GPIO_6);
    hi_io_set_func(HI_IO_NAME_GPIO_6, HI_IO_FUNC_GPIO_6_GPIO);
    IoTGpioSetDir(HI_IO_NAME_GPIO_6, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(HI_IO_NAME_GPIO_6, IOT_GPIO_VALUE0);  //CSN
    // CSN管脚拉低，延时250us(不要低于这个值)，给芯片上电
    // 在这里拉低GPIO_6到FM11_Init()，250us的延时已经满足，不需要再单独延时
    // hi_udelay(250);

    IoTGpioInit(HI_IO_NAME_GPIO_8);
    hi_io_set_func(HI_IO_NAME_GPIO_8, HI_IO_FUNC_GPIO_8_GPIO);
    IoTGpioSetDir(HI_IO_NAME_GPIO_8, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE1);  //IRQ_N
    //I2C0 already inited in maintask

    //osPriorityAboveNormal[32], osPriorityNormal[24]
    //{.name, .attr_bits, .cb_mem, .cb_size, .stack_mem, .stack_size, .priority, .tz_module, .reserved}
    osThreadAttr_t attr = {"NfcTask", 0, NULL, 0, NULL, 1024*4, 30, 0, 0};
    g_NfcTaskId = osThreadNew((osThreadFunc_t)NfcTask, NULL, &attr);
    if (g_NfcTaskId == NULL) {
        printf("[NfcTestEntry] Falied to create %s!\n", attr.name);
    }
}

